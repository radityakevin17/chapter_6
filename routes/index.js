const viewController = require('../controller/viewsController')
const ModelController = require('../controller/modelController')
const route = require('express').Router()

route.get('/', viewController.home)
route.get('/register', viewController.register)
route.post('/register', ModelController.register)


module.exports = route