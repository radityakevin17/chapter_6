const {user_games} = require("../models")
module.exports = class viewController {
    static async home(req,res) {
        try {
            const data = await user_games.findAll()
            res.render('./home', { data })
            
        } catch (error) {
            console.log(error)
            res.send('==error===')
        }

    }
    static async register(req,res){
        try {
            res.render('./register')
        } catch (error) {
            res.send('===error===')
            
        }
    }
}