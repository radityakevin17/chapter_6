const {user_games} = require('../models')

module.exports = class ModelController{

    static async register(req,res){
        try {
            const {name, user_id, password, role} = req.body
            const payload = {
                name, user_id, password, role
            }
            await user_games.create(payload)
            res.redirect('/')
            
        } catch (error) {
            console.log(error)
            res.send("===error===")
            
        }
    }

}